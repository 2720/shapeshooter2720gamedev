#include "../include/Display.h"
#include "../include/GameSimulator.h"
using namespace std;
int main() {
   Display disp;
   
   GameSimulator simul(disp, 30);
   simul.run();
}
