#ifndef SCENESIMULATOR_H
#define SCENESIMULATOR_H
#include <allegro5/allegro_primitives.h>
#include <list>
#include <memory>
#include "Simulator.h"
#include "Diamond.h"
class SceneSimulator: public Simulator {
  private:
   std::list<std::shared_ptr<Moveable>> mlist;
   std::list<std::shared_ptr<Drawable>> dlist;
  public:


   SceneSimulator(const Display &d, int fps, std::list<std::shared_ptr<Moveable>> ml, std::list<std::shared_ptr<Drawable>> dl) : Simulator(d, fps), mlist(ml), dlist(dl) {}


  
   void updateModel(double dt){
      for (std::list<std::shared_ptr<Moveable>>::iterator it=mlist.begin(); it!=mlist.end(); ++it)
      {(*it)->deltaMove(dt);}
   }
  
   void drawModel() {
      al_clear_to_color(al_map_rgb(0,0,0));
     for (std::list<std::shared_ptr<Drawable>>::iterator it=dlist.begin(); it!=dlist.end(); ++it)
      { (*it)->draw();}
     al_flip_display();
   }
   
};
#endif
