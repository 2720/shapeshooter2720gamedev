#ifndef LEVEL_H
#define LEVEL_H

///This interface provides a framework for functionality and ease of access for levels.
class Level {
  public :
   virtual ~Level(){};
   virtual bool isWon() = 0;
   virtual bool isLost() = 0;
   virtual bool isActive() = 0;
   virtual void setActive() = 0;
   virtual void setInActive() = 0;
   virtual void reset() = 0;
   virtual void lDraw() = 0;
   virtual void lMove(double period) = 0;
};
#endif
   
