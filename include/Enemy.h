#ifndef ENEMY_H
#define ENEMY_H
///This interface allows levels with multiple enemy types to call non-type specific functions.
class Enemy {
  public:
   virtual ~Enemy(){};
   virtual bool isDead() = 0; //checks if the enemy object is dead
   virtual void reset() = 0; //Resets the enemy
   virtual void setSpd(int i) = 0; ///setSpd() allows the programmer to change Enemy's speed when certain conditions are met.
   virtual double botHeight() = 0;///botHeight() is used to check for a game over.
};

#endif
