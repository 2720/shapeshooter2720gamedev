#ifndef TRAPEZOID_H
#define TRAPEZOID_H
#include "Enemy.h"
#include "Drawable.h"
#include "Moveable.h"
#include "Player.h"
#include <memory>
#include <allegro5/allegro_primitives.h>

//This class differentiates from Diamond in that it is a large boss with multiple hitboxes.
///h1-h5 represent individual hitboxes, all of which must be hit to kill Trapezoids.
class Trapezoid: public Drawable, public Moveable, public Enemy {
  private:
   Point origin;
   Vector heading;
   int speed;
   bool dead, h1, h2, h3, h4, h5;
   std::shared_ptr<Player> player;
  public:
   ~Trapezoid(){player.reset();}
   
  Trapezoid(std::shared_ptr<Player> p,int s=75) : player(p), origin(1596,116), heading(0,1), speed(s), dead(0), h1(0), h2(0), h3(0), h4(0), h5(0) {}
   void draw() {
      if (dead == 0){
	 al_draw_line(origin.x, origin.y, origin.x, origin.y+300,
		      al_map_rgb(0,0,255), 4);
	 al_draw_line(origin.x, origin.y+300, origin.x-80, origin.y+400,
		      al_map_rgb(0,0,255), 4);
	 al_draw_line(origin.x-80, origin.y+400, origin.x-80, origin.y-100,
		      al_map_rgb(0,0,255), 4);
	 al_draw_line(origin.x-80, origin.y-100, origin.x, origin.y,
		      al_map_rgb(0,0,255), 4);
	 if (!h1) {
	    al_draw_rectangle(origin.x-5, origin.y+5,
			      origin.x-15, origin.y+15,
			      al_map_rgb(255,0,0), 1);}
	 if (!h2) {
	    al_draw_rectangle(origin.x-35, origin.y+145,
			      origin.x-45, origin.y+155,
			      al_map_rgb(255,0,0), 1);}
	 if (!h3) {
	    al_draw_rectangle(origin.x-5, origin.y+290,
			      origin.x-15, origin.y+300,
			      al_map_rgb(255,0,0), 1);}
	 if (!h4) {
	    al_draw_rectangle(origin.x-60, origin.y-65,
			      origin.x-70, origin.y-55,
			      al_map_rgb(255,0,0), 1);}
	 if (!h5) {
	    al_draw_rectangle(origin.x-60, origin.y+355,
			      origin.x-70, origin.y+365,
			      al_map_rgb(255,0,0), 1);}
      }
   }
   ///deltamove() performs movement for Trapezoid, and calls HitDetection().
   void deltaMove(double p) {
      if (dead == 0) {
	 if(origin.y+400 >= 797)
	 {heading.y=-1; origin.x-=40;}
	 else if (origin.y-100 <= 3)
	 {heading.y=1; origin.x-=40;}
	 Point neworigin = origin + heading*speed*p;
	 origin = neworigin;
	 HitDetection();
	 if (origin.x-80 <= 300)
	 {dead = 1;}
      }
   }
   ///HitDetection() checks to see if Trapezoid's hitbox intersects with the players shots, and returns that information to the player if so, which deletes the shot.
   //@param void
   void HitDetection(){
      Point s1, s2, s3, s4, s5;
      s1 = player->shot1();
      s2 = player->shot2();
      s3 = player->shot3();
      s4 = player->shot4();
      s5 = player->shot5();
      if ((s1.x >= origin.x-15 && s1.x <= origin.x-5) &&
	  (s1.y >= origin.y+5 && s1.y <= origin.y+15)&& !h1)
      {h1 = 1; player->shotHit(1);}
      else if((s2.x >= origin.x-15 && s2.x <= origin.x-5) &&
	      (s2.y >= origin.y+5 && s2.y <= origin.y+15)&& !h1)
      {h1 = 1; player->shotHit(2);}
      else if((s3.x >= origin.x-15 && s3.x <= origin.x-5) &&
	      (s3.y >= origin.y+5 && s3.y <= origin.y+15)&& !h1)
      {h1 = 1; player->shotHit(3);}
      else if((s4.x >= origin.x-15 && s4.x <= origin.x-5) &&
	      (s4.y >= origin.y+5 && s4.y <= origin.y+15)&& !h1)
      {h1 = 1; player->shotHit(4);}
      else if((s5.x >= origin.x-15 && s5.x <= origin.x-5) &&
	      (s5.y >= origin.y+5 && s5.y <= origin.y+15)&& !h1)	 
      {h1 = 1; player->shotHit(5);}
      else if((s1.x >= origin.x-45 && s1.x <= origin.x-35) &&
	      (s1.y >= origin.y+145 && s1.y <= origin.y+155)&& !h2)
      {h2 = 1; player->shotHit(1);}
      else if((s2.x >= origin.x-45 && s2.x <= origin.x-35) &&
	      (s2.y >= origin.y+145 && s2.y <= origin.y+155)&& !h2)
      {h2 = 1; player->shotHit(2);}
      else if((s3.x >= origin.x-45 && s3.x <= origin.x-35) &&
	      (s3.y >= origin.y+145 && s3.y <= origin.y+155)&& !h2)
      {h2 = 1; player->shotHit(3);}
      else if((s4.x >= origin.x-45 && s4.x <= origin.x-35) &&
	      (s4.y >= origin.y+145 && s4.y <= origin.y+155)&& !h2)
      {h2 = 1; player->shotHit(4);}
      else if((s5.x >= origin.x-45 && s5.x <= origin.x-35) &&
	      (s5.y >= origin.y+145 && s5.y <= origin.y+155)&& !h2)
      {h2 = 1; player->shotHit(5);}
      else if((s1.x >= origin.x-15 && s1.x <= origin.x-5) &&
	      (s1.y >= origin.y+290 && s1.y <= origin.y+300)&& !h3)
      {h3 = 1; player->shotHit(1);}
      else if((s2.x >= origin.x-15 && s2.x <= origin.x-5) &&
	      (s2.y >= origin.y+290 && s2.y <= origin.y+300)&& !h3)
      {h3 = 1; player->shotHit(2);}
      else if((s3.x >= origin.x-15 && s3.x <= origin.x-5) &&
	      (s3.y >= origin.y+290 && s3.y <= origin.y+300)&& !h3)
      {h3 = 1; player->shotHit(3);}
      else if((s4.x >= origin.x-15 && s4.x <= origin.x-5) &&
	      (s4.y >= origin.y+290 && s4.y <= origin.y+300)&& !h3)
      {h3 = 1; player->shotHit(4);}
      else if((s5.x >= origin.x-15 && s5.x <= origin.x-5) &&
	      (s5.y >= origin.y+290 && s5.y <= origin.y+300)&& !h3)
      {h3 = 1; player->shotHit(5);}
      else if((s1.x >= origin.x-70 && s1.x <= origin.x-60) &&
	      (s1.y >= origin.y-65 && s1.y <= origin.y-55)&& !h4)
      {h4 = 1; player->shotHit(1);}
      else if((s2.x >= origin.x-70 && s2.x <= origin.x-60) &&
	      (s2.y >= origin.y-65 && s2.y <= origin.y-55)&& !h4)
      {h4 = 1; player->shotHit(2);}
      else if((s3.x >= origin.x-70 && s3.x <= origin.x-60) &&
	      (s3.y >= origin.y-65 && s3.y <= origin.y-55)&& !h4)
      {h4 = 1; player->shotHit(3);}
      else if((s4.x >= origin.x-70 && s4.x <= origin.x-60) &&
	      (s4.y >= origin.y-65 && s4.y <= origin.y-55)&& !h4)
      {h4 = 1; player->shotHit(4);}
      else if((s5.x >= origin.x-70 && s5.x <= origin.x-60) &&
	      (s5.y >= origin.y-65 && s5.y <= origin.y-55)&& !h4)
      {h4 = 1; player->shotHit(5);}
      else if((s1.x >= origin.x-70 && s1.x <= origin.x-60) &&
	      (s1.y >= origin.y+355 && s1.y <= origin.y+365)&& !h5)
      {h5 = 1; player->shotHit(1);}
      else if((s2.x >= origin.x-70 && s2.x <= origin.x-60) &&
	      (s2.y >= origin.y+355 && s2.y <= origin.y+365)&& !h5)
      {h5 = 1; player->shotHit(2);}
      else if((s3.x >= origin.x-70 && s3.x <= origin.x-60) &&
	      (s3.y >= origin.y+355 && s3.y <= origin.y+365)&& !h5)
      {h5 = 1; player->shotHit(3);}
      else if((s4.x >= origin.x-70 && s4.x <= origin.x-60) &&
	      (s4.y >= origin.y+355 && s4.y <= origin.y+365)&& !h5)
      {h5 = 1; player->shotHit(4);}
      else if((s5.x >= origin.x-70 && s5.x <= origin.x-60) &&
	      (s5.y >= origin.y+355 && s5.y <= origin.y+365)&& !h5)
      {h5 = 1; player->shotHit(5);}
      if (h1&&h2&&h3&&h4&&h5)
      {player->addScore(2000); dead = 1;}
   }
   //sets the speed
   //@param int
   //return void
   void setSpd(int i) {speed = i;} 

    //Checks if the enemy object is dead
   //@param void
   //return bool
   bool isDead() {return dead;} //Check if it is dead

    //function to check for a game over
   //@param void
   //return double
   double botHeight() {return origin.x-80;} //set the height of the object

   //Reset the object
   void reset() { //reset 
      heading.y=1; origin.x=1596; origin.y=116; dead = 0;
      h1=0; h2=0; h3=0; h4=0; h5=0;
   }

};

#endif

