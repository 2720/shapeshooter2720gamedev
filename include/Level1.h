#ifndef LEVEL1_H
#define LEVEL1_H
#include <cstdlib>
#include <memory>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <stdexcept>
#include <list>
#include "Level.h"
#include "Diamond.h"
#include "Player.h"
#include "Earth.h"

//Class handles all enemies and states for what is intended to be the first level.
///all Diamond enemies are created in constructor and are stored and accessed through an array.
class Level1: public Level {
  private:
   std::list<std::shared_ptr<Moveable>> ml;
   std::list<std::shared_ptr<Drawable>> dl;
   std::shared_ptr<Diamond> d[20];
   bool active;
   bool paused;
   bool lost;
   bool won;
   bool menu;
   bool nextStage;
   int enemyCount;
   int pausetimer;
   ALLEGRO_FONT *f1, *f2;
   ALLEGRO_KEYBOARD_STATE st;
   std::shared_ptr<Player> player;
   std::shared_ptr<Earth> earth;
  public:
   
   ~Level1(){
      player.reset(); earth.reset();
      for (int i=0; i<20; i++)
      {d[i].reset();}
      for (std::list<std::shared_ptr<Moveable>>::iterator it=ml.begin();
	   it!=ml.end(); ++it)
      {(*it).reset();}
      for (std::list<std::shared_ptr<Drawable>>::iterator itr=dl.begin();
	   itr!=dl.end(); ++itr)
      {(*itr).reset();} 
   }
      
  Level1(std::shared_ptr<Player> pl) : enemyCount(20), player(pl), active(0), paused(0), pausetimer(0), won(0), lost(0), nextStage(0), menu(0) {
      std::shared_ptr<Earth> gaia(new Earth);
      earth = gaia;
      std::shared_ptr<Drawable> edraw = earth;
      dl.push_back(edraw);
      
      std::shared_ptr<Drawable> pdraw = player;
      std::shared_ptr<Moveable> pmove = player;
      dl.push_back(pdraw);
      ml.push_back(pmove);
	 
      auto e1 = std::make_shared<Diamond>(player);
      std::shared_ptr<Drawable> edraw1 = e1;
      std::shared_ptr<Moveable> emove1 = e1;
      d[0] = e1;
      dl.push_back(edraw1);
      ml.push_back(emove1);

      auto e2 = std::make_shared<Diamond>(player,1);
      std::shared_ptr<Drawable> edraw2 = e2;
      std::shared_ptr<Moveable> emove2 = e2;
      d[1] = e2;
      dl.push_back(edraw2);
      ml.push_back(emove2);

      auto e3 = std::make_shared<Diamond>(player,2);
      std::shared_ptr<Drawable> edraw3 = e3;
      std::shared_ptr<Moveable> emove3 = e3;
      d[2] = e3;
      dl.push_back(edraw3);
      ml.push_back(emove3);

      auto e4 = std::make_shared<Diamond>(player,3);
      std::shared_ptr<Drawable> edraw4 = e4;
      std::shared_ptr<Moveable> emove4 = e4;
      d[3] = e4;
      dl.push_back(edraw4);
      ml.push_back(emove4);

      auto e5 = std::make_shared<Diamond>(player,4);
      std::shared_ptr<Drawable> edraw5 = e5;
      std::shared_ptr<Moveable> emove5 = e5;
      d[4] = e5;
      dl.push_back(edraw5);
      ml.push_back(emove5);

      auto e6 = std::make_shared<Diamond>(player,5);
      std::shared_ptr<Drawable> edraw6 = e6;
      std::shared_ptr<Moveable> emove6 = e6;
      d[5] = e6;
      dl.push_back(edraw6);
      ml.push_back(emove6);

      auto e7 = std::make_shared<Diamond>(player,6);
      std::shared_ptr<Drawable> edraw7 = e7;
      std::shared_ptr<Moveable> emove7 = e7;
      d[6] = e7;
      dl.push_back(edraw7);
      ml.push_back(emove7);

      auto e8 = std::make_shared<Diamond>(player,7);
      std::shared_ptr<Drawable> edraw8 = e8;
      std::shared_ptr<Moveable> emove8 = e8;
      d[7] = e8;
      dl.push_back(edraw8);
      ml.push_back(emove8);

      auto e9 = std::make_shared<Diamond>(player,8);
      std::shared_ptr<Drawable> edraw9 = e9;
      std::shared_ptr<Moveable> emove9 = e9;
      d[8] = e9;
      dl.push_back(edraw9);
      ml.push_back(emove9);

      auto e10 = std::make_shared<Diamond>(player,9);
      std::shared_ptr<Drawable> edraw10 = e10;
      std::shared_ptr<Moveable> emove10 = e10;
      d[9] = e10;
      dl.push_back(edraw10);
      ml.push_back(emove10);

      auto e11 = std::make_shared<Diamond>(player,0,1);
      std::shared_ptr<Drawable> edraw11 = e11;
      std::shared_ptr<Moveable> emove11 = e11;
      d[10] = e11;
      dl.push_back(edraw11);
      ml.push_back(emove11);
      
      auto e12 = std::make_shared<Diamond>(player,1,1);
      std::shared_ptr<Drawable> edraw12 = e12;
      std::shared_ptr<Moveable> emove12 = e12;
      d[11] = e12;
      dl.push_back(edraw12);
      ml.push_back(emove12);

      auto e13 = std::make_shared<Diamond>(player,2,1);
      std::shared_ptr<Drawable> edraw13 = e13;
      std::shared_ptr<Moveable> emove13 = e13;
      d[12] = e13;
      dl.push_back(edraw13);
      ml.push_back(emove13);

      auto e14 = std::make_shared<Diamond>(player,3,1);
      std::shared_ptr<Drawable> edraw14 = e14;
      std::shared_ptr<Moveable> emove14 = e14;
      d[13] = e14;
      dl.push_back(edraw14);
      ml.push_back(emove14);

      auto e15 = std::make_shared<Diamond>(player,4,1);
      std::shared_ptr<Drawable> edraw15 = e15;
      std::shared_ptr<Moveable> emove15 = e15;
      d[14] = e15;
      dl.push_back(edraw15);
      ml.push_back(emove15);

      auto e16 = std::make_shared<Diamond>(player,5,1);
      std::shared_ptr<Drawable> edraw16 = e16;
      std::shared_ptr<Moveable> emove16 = e16;
      d[15] = e16;
      dl.push_back(edraw16);
      ml.push_back(emove16);

      auto e17 = std::make_shared<Diamond>(player,6,1);
      std::shared_ptr<Drawable> edraw17 = e17;
      std::shared_ptr<Moveable> emove17 = e17;
      d[16] = e17;
      dl.push_back(edraw17);
      ml.push_back(emove17);

      auto e18 = std::make_shared<Diamond>(player,7,1);
      std::shared_ptr<Drawable> edraw18 = e18;
      std::shared_ptr<Moveable> emove18 = e18;
      d[17] = e18;
      dl.push_back(edraw18);
      ml.push_back(emove18);

      auto e19 = std::make_shared<Diamond>(player,8,1);
      std::shared_ptr<Drawable> edraw19 = e19;
      std::shared_ptr<Moveable> emove19 = e19;
      d[18] = e19;
      dl.push_back(edraw19);
      ml.push_back(emove19);

      auto e20 = std::make_shared<Diamond>(player,9,1);
      std::shared_ptr<Drawable> edraw20 = e20;
      std::shared_ptr<Moveable> emove20 = e20;
      d[19] = e20;
      dl.push_back(edraw20);
      ml.push_back(emove20);
      
      al_init_font_addon();
      al_init_ttf_addon();
      f1 = al_load_ttf_font("Bitwise.ttf",30,0);
      if(!f1) {
	 throw std::runtime_error("Cannot load 'Bitwise.ttf'");
      }
      f2 = al_load_ttf_font("Bitwise.ttf",60,0);
      if(!f2) {
	 throw std::runtime_error("Cannot load 'Bitwise.ttf'");
      }
   }
   ///bools won and lost are temporary states which require user response to continue from.
   bool isLost() {return menu;}
   bool isWon() {return nextStage;}
   bool isActive() {return active;}
   void setActive() {active = true;}
   void setInActive() {active = false;}
   void reset() { active=0; nextStage=0; enemyCount=20; won=0; lost=0; menu=0;
      for (int i=0; i<20; i++)
      {d[i]->reset();}
   }

   ///ldraw calls all Drawable objects draw() functions, as well as any text that needs to be drawn, depending on state.
   void lDraw() {
      al_draw_text(f1,al_map_rgb(200,200,200),100,30,
		   ALLEGRO_ALIGN_CENTRE,"Score :");
      al_draw_textf(f1,al_map_rgb(200,200,200),155,30,
		    ALLEGRO_ALIGN_LEFT, "%i", player->getScore());
      al_draw_text(f1,al_map_rgb(200,200,200),1525,30,
		   ALLEGRO_ALIGN_RIGHT,"Enemies Left :");
      al_draw_textf(f1,al_map_rgb(200,200,200),1525,30,
		    ALLEGRO_ALIGN_LEFT, "%i",enemyCount);

      for (std::list<std::shared_ptr<Drawable>>::iterator it=dl.begin();
	   it!=dl.end(); ++it)
      { (*it)->draw();}
      if (paused) {
	 al_draw_text(f2,al_map_rgb(200,200,200),800,300,
		      ALLEGRO_ALIGN_CENTRE,"PAUSED");
      }
      if (won && !lost) {
	 al_draw_text(f2,al_map_rgb(200,200,200),800,300,
		      ALLEGRO_ALIGN_CENTRE,"LEVEL COMPLETE"); }
      if (lost) {
	 al_draw_text(f2,al_map_rgb(200,200,200),800,300,
		      ALLEGRO_ALIGN_CENTRE,"GAME OVER");
	 al_draw_text(f1,al_map_rgb(200,200,200),800,500,
		      ALLEGRO_ALIGN_CENTRE,"Press space to continue");}
   }

   ///lmove calls all Moveable objects deltaMove() functions, performs any user interactions that are not related to the player object (pausing, continuing to next level, returning to main menu), and updates each enemy on their current position in regards to the array.
   void lMove(double period) {
      int ec = 0;
      if (!paused && !won && !lost) {
	 for (std::list<std::shared_ptr<Moveable>>::iterator it=ml.begin();
	      it!=ml.end(); ++it)
	 {(*it)->deltaMove(period);}  
	 for (int i = 0; i<20;i++){
	    if (!d[i]->isDead()) {ec++;}
	    if (d[i]->botHeight() <= 300)
	    {lost=1;}
	 }
	 if (d[9]->isDead()&&d[19]->isDead()){
	    if (d[8]->isDead()&&d[18]->isDead()){
	       if(d[7]->isDead()&&d[17]->isDead()){
		  if(d[6]->isDead()&&d[16]->isDead()){
		     if(d[5]->isDead()&&d[15]->isDead()){
			if(d[4]->isDead()&&d[14]->isDead()){
			   if(d[3]->isDead()&&d[13]->isDead()){
			      if(d[2]->isDead()&&d[12]->isDead()){
				 if(d[1]->isDead()&&d[11]->isDead()){
				    d[0]->setRight(0);d[10]->setRight(0);}
				 else {
				    d[1]->setRight(0);d[11]->setRight(0);
				    d[0]->setRight(1);d[10]->setRight(1);}}
			      else {
				 d[2]->setRight(0);d[12]->setRight(0);
				 d[1]->setRight(1);d[11]->setRight(1);
				 d[0]->setRight(2);d[10]->setRight(2);}}
			   else {
			      d[3]->setRight(0);d[13]->setRight(0);
			      d[2]->setRight(1);d[12]->setRight(1);
			      d[1]->setRight(2);d[11]->setRight(2);
			      d[0]->setRight(3);d[10]->setRight(3);}}
			else {
			   d[4]->setRight(0);d[14]->setRight(0);
			   d[3]->setRight(1);d[13]->setRight(1);
			   d[2]->setRight(2);d[12]->setRight(2);
			   d[1]->setRight(3);d[11]->setRight(3);
			   d[0]->setRight(4);d[10]->setRight(4);}}
		     else {
			d[5]->setRight(0);d[15]->setRight(0);
			d[4]->setRight(1);d[14]->setRight(1);
			d[3]->setRight(2);d[13]->setRight(2);
			d[2]->setRight(3);d[12]->setRight(3);
			d[1]->setRight(4);d[11]->setRight(4);
			d[0]->setRight(5);d[10]->setRight(5);}}
		  else {
		     d[6]->setRight(0);d[16]->setRight(0);
		     d[5]->setRight(1);d[15]->setRight(1);
		     d[4]->setRight(2);d[14]->setRight(2);
		     d[3]->setRight(3);d[13]->setRight(3);
		     d[2]->setRight(4);d[12]->setRight(4);
		     d[1]->setRight(5);d[11]->setRight(5);
		     d[0]->setRight(6);d[10]->setRight(6);}}
	       else {
		  d[7]->setRight(0);d[17]->setRight(0);
		  d[6]->setRight(1);d[16]->setRight(1);
		  d[5]->setRight(2);d[15]->setRight(2);
		  d[4]->setRight(3);d[14]->setRight(3);
		  d[3]->setRight(4);d[13]->setRight(4);
		  d[2]->setRight(5);d[12]->setRight(5);
		  d[1]->setRight(6);d[11]->setRight(6);
		  d[0]->setRight(7);d[10]->setRight(7);}}
	    else {
	       d[8]->setRight(0);d[18]->setRight(0);
	       d[7]->setRight(1);d[17]->setRight(1);
	       d[6]->setRight(2);d[16]->setRight(2);
	       d[5]->setRight(3);d[15]->setRight(3);
	       d[4]->setRight(4);d[14]->setRight(4);
	       d[3]->setRight(5);d[13]->setRight(5);
	       d[2]->setRight(6);d[12]->setRight(6);
	       d[1]->setRight(7);d[11]->setRight(7);
	       d[0]->setRight(8);d[10]->setRight(8);}}
	 
	 if (d[0]->isDead()&&d[10]->isDead()){
	    if (d[1]->isDead()&&d[11]->isDead()){
	       if(d[2]->isDead()&&d[12]->isDead()){
		  if(d[3]->isDead()&&d[13]->isDead()){
		     if(d[4]->isDead()&&d[14]->isDead()){
			if(d[5]->isDead()&&d[15]->isDead()){
			   if(d[6]->isDead()&&d[16]->isDead()){
			      if(d[7]->isDead()&&d[17]->isDead()){
				 if(d[8]->isDead()&&d[18]->isDead()){
				    d[9]->setLeft(0); d[19]->setLeft(0);}
				 else {
				    d[9]->setLeft(1); d[19]->setLeft(1);
				    d[8]->setLeft(0); d[18]->setLeft(0);}}
			      else {
				 d[9]->setLeft(2); d[19]->setLeft(2);
				 d[8]->setLeft(1); d[18]->setLeft(1);
				 d[7]->setLeft(0); d[17]->setLeft(0);}}
			   else {
			      d[9]->setLeft(3); d[19]->setLeft(3);
			      d[8]->setLeft(2); d[18]->setLeft(2);
			      d[7]->setLeft(1); d[17]->setLeft(1);
			      d[6]->setLeft(0); d[16]->setLeft(0);}}
			else{
			   d[9]->setLeft(4); d[19]->setLeft(4);
			   d[8]->setLeft(3); d[18]->setLeft(3);
			   d[7]->setLeft(2); d[17]->setLeft(2);
			   d[6]->setLeft(1); d[16]->setLeft(1);
			   d[5]->setLeft(0); d[15]->setLeft(0);}}
		     else {
			d[9]->setLeft(5); d[19]->setLeft(5);
			d[8]->setLeft(4); d[18]->setLeft(4);
			d[7]->setLeft(3); d[17]->setLeft(3);
			d[6]->setLeft(2); d[16]->setLeft(2);
			d[5]->setLeft(1); d[15]->setLeft(1);
			d[4]->setLeft(0); d[14]->setLeft(0);}}
		  else {
		     d[9]->setLeft(6); d[19]->setLeft(6);
		     d[8]->setLeft(5); d[18]->setLeft(5);
		     d[7]->setLeft(4); d[17]->setLeft(4);
		     d[6]->setLeft(3); d[16]->setLeft(3);
		     d[5]->setLeft(2); d[15]->setLeft(2);
		     d[4]->setLeft(1); d[14]->setLeft(1);
		     d[3]->setLeft(0); d[13]->setLeft(0);}}
	       else {
		  d[9]->setLeft(7); d[19]->setLeft(7);
		  d[8]->setLeft(6); d[18]->setLeft(6);
		  d[7]->setLeft(5); d[17]->setLeft(5);
		  d[6]->setLeft(4); d[16]->setLeft(4);
		  d[5]->setLeft(3); d[15]->setLeft(3);
		  d[4]->setLeft(2); d[14]->setLeft(2);
		  d[3]->setLeft(1); d[13]->setLeft(1);
		  d[2]->setLeft(0); d[12]->setLeft(0);}}
	    else {
	       d[9]->setLeft(8); d[19]->setLeft(8);
	       d[8]->setLeft(7); d[18]->setLeft(7);
	       d[7]->setLeft(6); d[17]->setLeft(6);
	       d[6]->setLeft(5); d[16]->setLeft(5);
	       d[5]->setLeft(4); d[15]->setLeft(4);
	       d[4]->setLeft(3); d[14]->setLeft(3);
	       d[3]->setLeft(2); d[13]->setLeft(2);
	       d[2]->setLeft(1); d[12]->setLeft(1);
	       d[1]->setLeft(0); d[11]->setLeft(0);}}
	 enemyCount=ec;
	 if (ec==0) {won = true;}
      }
      al_get_keyboard_state(&st);
      if (al_key_down(&st,ALLEGRO_KEY_SPACE) && lost)
      {menu = true;}
      if (al_key_down(&st,ALLEGRO_KEY_ENTER) && won)
      {nextStage=true;}
      if (al_key_down(&st,ALLEGRO_KEY_ESCAPE)&& pausetimer > 10)
      {paused = !paused; pausetimer = 0;}
      pausetimer++;
   }
};

#endif
