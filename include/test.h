#ifndef LEVEL1_H
#define LEVEL1_H
#include <cstdlib>
#include <memory>
#include <list>
#include "Level.h"
#include "Diamond.h"

class Level1: public Level {
  private:
   std::list<std::shared_ptr<Moveable>> ml;
   std::list<std::shared_ptr<Drawable>> dl;
   std::list<std::shared_ptr<Enemy>> el;
   int enemyCount;
  public:
  Level1() : enemyCount(1) {
      auto e1 = std::make_shared<Diamond>(100);
      std::shared_ptr<Drawable> edraw1 = e1;
      std::shared_ptr<Moveable> emove1 = e1;
      std::shared_ptr<Enemy> d1 = e1;
      dl.push_back(edraw1);
      ml.push_back(emove1);
      el.push_back(d1);
      }
   
   bool isWon() {return enemyCount==0;}
   bool isLost() {
      for (std::list<std::shared_ptr<Enemy>>::iterator it=el.begin();
	   it!=el.end(); ++it)
      {if ((*it)->botHeight() <= 300)
	 {return true;}
      }
   }
   void lDraw() {
      if (isWon())
      {}
      else if (isLost()) {al_rest(60.0);}
      else {
	 for (std::list<std::shared_ptr<Drawable>>::iterator it=dl.begin();
	      it!=dl.end(); ++it)
	 { (*it)->draw();}
      }
   }
   void lMove(double period) {
      for (std::list<std::shared_ptr<Moveable>>::iterator it=ml.begin();
	   it!=ml.end(); ++it)
      {(*it)->deltaMove(period);}
   }
};

#endif
