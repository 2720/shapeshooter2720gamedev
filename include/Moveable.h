#ifndef MOVEABLE_H
#define MOVEABLE_H
///This interface allows the objects drawn on screen to animate.
class Moveable {
  public:
   virtual ~Moveable() {};
   virtual void deltaMove(double framePeriod) = 0;
};
#endif
