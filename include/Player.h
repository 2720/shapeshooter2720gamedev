#ifndef PLAYER_H
#define PLAYER_H
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro.h>
#include "Drawable.h"
#include "Moveable.h"
///class handles the players drawing and movement, the drawing and movement of the players shots, and the handling of some global elements which extend beyond levels, but reset with the player, such as score.
struct Vector {
   double x, y;
   Vector(double xi=0.0, double yi=0.0) {x=xi;y=yi;}
   Vector operator *(double m) {return Vector(x*m, y*m);}
};
///Structure to store coordinates 
//@param double
struct Point {
   double x, y;
   Point(double xi=0.0, double yi=0.0) {x=xi;y=yi;}
   Point operator +(Vector a) {return Point(x+a.x, y+a.y);}
};
///class player to Handle Player and player shots
class Player: public Moveable, public Drawable {
  private:
   Point origin, so1, so2, so3, so4, so5;
   Vector heading, sh;
   int speed, shotCount, totalShots, score, multiplier;
   bool s1, s2, s3, s4, s5;
  public:
  Player(int s=200) : origin(100, 390), heading(0,0), speed(s), shotCount(0), sh(1,0), s1(0), s2(0), s3(0), s4(0), s5(0), totalShots(3), multiplier(1), score(0) {}

   void draw() {
//Draws Player
      al_draw_line(origin.x,origin.y,origin.x,origin.y+20,
		   al_map_rgb(0,255,0),3);
      al_draw_line(origin.x,origin.y+20,origin.x-20,origin.y+20,
		   al_map_rgb(0,255,0),3);
      al_draw_line(origin.x-20,origin.y+20,origin.x-20,origin.y+40,
		   al_map_rgb(0,255,0),3);
      al_draw_line(origin.x-20,origin.y+40,origin.x-40,origin.y+40,
		   al_map_rgb(0,255,0),3);
      al_draw_line(origin.x-40,origin.y+40,origin.x-40,origin.y-20,
		   al_map_rgb(0,255,0),3);
      al_draw_line(origin.x-40,origin.y-20,origin.x-20,origin.y-20,
		   al_map_rgb(0,255,0),3);
      al_draw_line(origin.x-20,origin.y-20,origin.x-20,origin.y,
		   al_map_rgb(0,255,0),3);
      al_draw_line(origin.x-20,origin.y,origin.x,origin.y,
		   al_map_rgb(0,255,0),3);
//Draws shots
      if (s1 == true)
      {al_draw_triangle(so1.x,so1.y,so1.x-4,so1.y-2,so1.x-4,so1.y+2,
			al_map_rgb(200,200,200),1);}
      if (s2 == true)
      {al_draw_triangle(so2.x,so2.y,so2.x-4,so2.y-2,so2.x-4,so2.y+2,
			al_map_rgb(200,200,200),1);}
      if (s3 == true)
      {al_draw_triangle(so3.x,so3.y,so3.x-4,so3.y-2,so3.x-4,so3.y+2,
			al_map_rgb(200,200,200),1);}
      if (s4 == true)
      {al_draw_triangle(so4.x,so4.y,so4.x-4,so4.y-2,so4.x-4,so4.y+2,
			al_map_rgb(200,200,200),1);}
      if (s5 == true)
      {al_draw_triangle(so5.x,so5.y,so5.x-4,so5.y-2,so5.x-4,so5.y+2,
			al_map_rgb(200,200,200),1);}
   }

   Point shot1() const {return so1;} 
   Point shot2() const {return so2;}
   Point shot3() const {return so3;}
   Point shot4() const {return so4;}
   Point shot5() const {return so5;}
   
   void deltaMove(double framePeriod) {
      ALLEGRO_KEYBOARD_STATE st;
      al_get_keyboard_state(&st);
//Movement and key detection for player
      if (al_key_down(&st, ALLEGRO_KEY_UP) && origin.y >= 20 && origin.x <= 295 && origin.x >= 40)
      {heading.y=-1;}
      else if (al_key_down(&st, ALLEGRO_KEY_DOWN) && origin.y <= 760 && origin.x <= 295 && origin.x >= 40)
      {heading.y=1;}
      else if (al_key_down(&st, ALLEGRO_KEY_LEFT) && origin.x >= 40 && origin.y <= 760 && origin.y >= 20)
      {heading.x=-1;}
      else if (al_key_down(&st, ALLEGRO_KEY_RIGHT) && origin.x <= 295 && origin.y <= 760 && origin.y >= 20)
      {heading.x=1;}
      else
      {heading.x=0; heading.y=0;}
      Point neworigin = origin + heading*speed*framePeriod;
      origin = neworigin;
//logic for shots
      if (al_key_down(&st, ALLEGRO_KEY_SPACE) && (shotCount==0 || s2==true && s3==true) && s1==false)
      {s1 = true; so1.x=origin.x; so1.y=origin.y+10; shotCount++;}
      else if (al_key_down(&st, ALLEGRO_KEY_SPACE) && shotCount==1 && s1==true && so1.x >= origin.x+40 && s2 == false)
      {s2 = true; so2.x=origin.x; so2.y=origin.y+10; shotCount++;}
      else if (al_key_down(&st, ALLEGRO_KEY_SPACE) && shotCount==2 &&
	       ((so2.x >= origin.x+40 && so1.x >= origin.x+80) || (s1==false &&s2==true)) && s3==false)
      {s3 = true; so3.x=origin.x; so3.y=origin.y+10; shotCount++;}
      else if (al_key_down(&st, ALLEGRO_KEY_SPACE) && shotCount==3 &&
	       so3.x >= origin.x+40 && so2.x >= origin.x+80 && totalShots>=4
	       && s4==false)
      {s4 = true; so4.x=origin.x; so4.y=origin.y+10; shotCount++;}
      else if (al_key_down(&st, ALLEGRO_KEY_SPACE) && shotCount==4 && so4.x >= origin.x+40 && so3.x >= origin.x+80 && totalShots==5 && s5==false)
      {s5 = true; so5.x=origin.x; so5.y=origin.y+10; shotCount++;}
      if (s1==true && so1.x >= 1600) {s1=false; shotCount--;}
      if (s2==true && so2.x >= 1600) {s2=false; shotCount--;}
      if (s3==true && so3.x >= 1600) {s3=false; shotCount--;}
      if (s4==true && so4.x >= 1600) {s4=false; shotCount--;}
      if (s5==true && so5.x >= 1600) {s5=false; shotCount--;}
//shot movement
      Point nso1 = so1 + sh*300*framePeriod;
      Point nso2 = so2 + sh*300*framePeriod;
      Point nso3 = so3 + sh*300*framePeriod;
      Point nso4 = so4 + sh*300*framePeriod;
      Point nso5 = so5 + sh*300*framePeriod;
      so1 = nso1; so2 = nso2; so3 = nso3; so4 = nso4; so5 = nso5;
   }
   //adds shots 
   void addShot() {
      if (totalShots < 5) {totalShots++;}
   }
   //function to reset() the shots available
   //return void
   void reset() {
      totalShots = 3;
      score = 0;
      origin.x = 100; origin.y = 390;
      s1 = false; s2 = false; s3 = false; s4 = false; s5 = false;
      speed = 200;
      multiplier = 1;
   }
   //Function to Compute thescore 
   void multUp() {multiplier++;}
   //funtion 
   int getMult() {return multiplier;}
   void addScore(int sc) {score += sc*multiplier;}
   //gets the scores
   int getScore() {return score;}
   //gets the total score
   int getTotalShots() {return totalShots;}
   //produces the shots
   void shotHit(int i) {
      switch (i) {
	 case 1: s1 = false; so1.x = 1600; shotCount--; break;
	 case 2: s2 = false; so2.x = 1600; shotCount--; break;
	 case 3: s3 = false; so3.x = 1600; shotCount--; break;
	 case 4: s4 = false; so4.x = 1600; shotCount--; break;
	 case 5: s5 = false; so5.x = 1600; shotCount--; break;
      }
   }
};

#endif
