#ifndef LEVEL3_H
#define LEVEL3_H
#include <cstdlib>
#include <memory>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <stdexcept>
#include <list>
#include "Level.h"
#include "Trapezoid.h"
#include "Player.h"
#include "Earth.h"

///This class handles all enemies and states for what is intended to be the third level.
class Level3: public Level {
  private:
   std::list<std::shared_ptr<Moveable>> ml;
   std::list<std::shared_ptr<Drawable>> dl;
   std::shared_ptr<Trapezoid> t;
   bool active;
   bool paused;
   bool lost;
   bool won;
   bool menu;
   bool nextStage;
   int enemyCount;
   int pausetimer;
   ALLEGRO_FONT *f1, *f2;
   ALLEGRO_KEYBOARD_STATE st;
   std::shared_ptr<Player> player;
   std::shared_ptr<Earth> earth;
  public:
   
   ~Level3(){
      player.reset(); earth.reset(); t.reset();
      for (std::list<std::shared_ptr<Moveable>>::iterator it=ml.begin();
	   it!=ml.end(); ++it)
      {(*it).reset();}
      for (std::list<std::shared_ptr<Drawable>>::iterator itr=dl.begin();
	   itr!=dl.end(); ++itr)
      {(*itr).reset();} 
   }
   
  Level3(std::shared_ptr<Player> pl) : enemyCount(1), player(pl), active(0), paused(0), pausetimer(0), won(0), lost(0), nextStage(0), menu(0) {
      std::shared_ptr<Earth> gaia(new Earth);
      earth = gaia;
      std::shared_ptr<Drawable> edraw = earth;
      dl.push_back(edraw);
      
      std::shared_ptr<Drawable> pdraw = player;
      std::shared_ptr<Moveable> pmove = player;
      dl.push_back(pdraw);
      ml.push_back(pmove);
	 
      auto e1 = std::make_shared<Trapezoid>(player);
      std::shared_ptr<Drawable> edraw1 = e1;
      std::shared_ptr<Moveable> emove1 = e1;
      t = e1;
      dl.push_back(edraw1);
      ml.push_back(emove1);
      
      al_init_font_addon();
      al_init_ttf_addon();
      f1 = al_load_ttf_font("Bitwise.ttf",30,0);
      if(!f1) {
	 throw std::runtime_error("Cannot load 'Bitwise.ttf'");
      }
      f2 = al_load_ttf_font("Bitwise.ttf",60,0);
      if(!f2) {
	 throw std::runtime_error("Cannot load 'Bitwise.ttf'");
      }
   }
   bool isLost() {return menu;}
   bool isWon() {return nextStage;}
   bool isActive() {return active;}
   void setActive() {active = true;}
   void setInActive() {active = false;}
   void reset() { active=0; nextStage=0; enemyCount=20; won=0; lost=0; menu=0;
      t->reset();
   }
   void lDraw() {
      al_draw_text(f1,al_map_rgb(200,200,200),100,30,
		   ALLEGRO_ALIGN_CENTRE,"Score :");
      al_draw_textf(f1,al_map_rgb(200,200,200),155,30,
		    ALLEGRO_ALIGN_LEFT, "%i", player->getScore());
      al_draw_text(f1,al_map_rgb(200,200,200),1525,30,
		   ALLEGRO_ALIGN_RIGHT,"BOSS :");
      al_draw_textf(f1,al_map_rgb(200,200,200),1525,30,
		    ALLEGRO_ALIGN_LEFT, "%i",enemyCount);

      for (std::list<std::shared_ptr<Drawable>>::iterator it=dl.begin();
	   it!=dl.end(); ++it)
      { (*it)->draw();}
      if (paused) {
	 al_draw_text(f2,al_map_rgb(200,200,200),800,300,
		      ALLEGRO_ALIGN_CENTRE,"PAUSED");
      }
      if (won && !lost) {
	 al_draw_text(f2,al_map_rgb(200,200,200),800,300,
		      ALLEGRO_ALIGN_CENTRE,"LEVEL COMPLETE"); }
      if (lost) {
	 al_draw_text(f2,al_map_rgb(200,200,200),800,300,
		      ALLEGRO_ALIGN_CENTRE,"GAME OVER");
	 al_draw_text(f1,al_map_rgb(200,200,200),800,500,
		      ALLEGRO_ALIGN_CENTRE,"Press space to continue");}
   }
   void lMove(double period) {
      int ec = 0;
      if (!paused && !won && !lost) {
	 for (std::list<std::shared_ptr<Moveable>>::iterator it=ml.begin();
	      it!=ml.end(); ++it)
	 {(*it)->deltaMove(period);}  
	 if (!t->isDead()) {ec++;}
	 if (t->botHeight() <= 300)
	 {lost=1;}
	 enemyCount=ec;
	 if (ec==0) {won = true;}
      }
      al_get_keyboard_state(&st);
      if (al_key_down(&st,ALLEGRO_KEY_SPACE) && lost)
      {menu = true;}
      if (al_key_down(&st,ALLEGRO_KEY_ENTER) && won)
      {nextStage=true;}
      if (al_key_down(&st,ALLEGRO_KEY_ESCAPE)&& pausetimer > 10)
      {paused = !paused; pausetimer = 0;}
      pausetimer++;
   }
};

#endif
