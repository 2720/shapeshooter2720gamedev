#ifndef EARTH_H
#define EARTH_H
#include "Drawable.h"
///This class draws a line which represents both the player's movement area and the point at which the enemies win.
class Earth: public Drawable {
  public:
   Earth() {}
   void draw () {
      al_draw_line(300,800,300,0,al_map_rgb(0,200,200),4);
   }
};

#endif
