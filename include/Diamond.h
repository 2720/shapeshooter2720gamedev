#ifndef DIAMOND_H
#define DIAMOND_H
#include "Enemy.h"
#include "Drawable.h"
#include "Moveable.h"
#include "Player.h"
#include <memory>
#include <allegro5/allegro_primitives.h>
///This class handles the drawing, movement, and relevant functions of the Diamond enemy type, such as hit detection and detecting its position relative to other Diamond enemies.
class Diamond: public Drawable, public Moveable, public Enemy {
  private:
   Point origin;
   Vector heading;
   int speed, column, row, rightmost, leftmost;
   bool dead;
   std::shared_ptr<Player> player;
  public:
  Diamond(std::shared_ptr<Player> p,int col=0,int r=0,int s=100) : row(r), column(col),  player(p), origin(1596-(40*r),16+(28*col)), heading(0,1), speed(s), dead(0), rightmost(9-col), leftmost(0+col) {}
   ~Diamond() {player.reset();}
   
   ///draw() draws Diamond while it is not dead.
   void draw() {
      if (dead == 0){
	 al_draw_line(origin.x, origin.y, origin.x-18, origin.y+12,
		      al_map_rgb(0,0,255), 2);
	 al_draw_line(origin.x-18, origin.y+12, origin.x-36, origin.y,
		      al_map_rgb(0,0,255), 2);
	 al_draw_line(origin.x-36, origin.y, origin.x-18, origin.y-12,
		      al_map_rgb(0,0,255), 2);
	 al_draw_line(origin.x-18, origin.y-12, origin.x, origin.y,
		      al_map_rgb(0,0,255), 2);
	 al_draw_rectangle(origin.x-12, origin.y+5,
			   origin.x-22, origin.y-5,
			al_map_rgb(255,0,0), 1);
      }
   }

   ///deltamove() performs movement for Diamond, and calls HitDetection().
   void deltaMove(double p) {
      if (dead == 0) {
	 if(origin.y+12+28*rightmost >= 797)
	 {heading.y=-1; origin.x-=36;}
	 else if (origin.y-12-28*leftmost <= 3)
	 {heading.y=1; origin.x-=36;}
	 Point neworigin = origin + heading*speed*p;
	 origin = neworigin;
	 HitDetection();
	 if (origin.x-36 <= 300)
	 {dead = 1;}
      }
   }

   ///HitDetection() checks to see if Diamond's hitbox intersects with the players shots, and returns that information to the player if so, which deletes the shot.
   //@param is void
   void HitDetection(){
      Point s1, s2, s3, s4, s5;
      s1 = player->shot1();
      s2 = player->shot2();
      s3 = player->shot3();
      s4 = player->shot4();
      s5 = player->shot5();
      if ((s1.x >= origin.x-22 && s1.x <= origin.x-12) &&
	  (s1.y >= origin.y-5 && s1.y <= origin.y+5))
      {dead = 1; player->addScore(200); player->shotHit(1);}
      else if((s2.x >= origin.x-22 && s2.x <= origin.x-12) &&
       (s2.y >= origin.y-5 && s2.y <= origin.y+5))
      {dead = 1; player->addScore(200); player->shotHit(2);}
      else if((s3.x >= origin.x-22 && s3.x <= origin.x-12) &&
       (s3.y >= origin.y-5 && s3.y <= origin.y+5))
      {dead = 1; player->addScore(200); player->shotHit(3);}
      else if((s4.x >= origin.x-22 && s4.x <= origin.x-12) &&
       (s4.y >= origin.y-5 && s4.y <= origin.y+5))
      {dead = 1; player->addScore(200); player->shotHit(4);}
      else if((s5.x >= origin.x-22 && s5.x <= origin.x-12) &&
       (s5.y >= origin.y-5 && s5.y <= origin.y+5))
      {dead = 1; player->addScore(200); player->shotHit(5);}
   }

   void setSpd(int i) {speed = i;}///setSpd() allows the programmer to change Enemy's speed when certain conditions are met.

   bool isDead() {return dead;} //checks if the enemy object is dead

   double botHeight() {return origin.x-36;}///botHeight() is used to check for a game over.

   void setLeft(int i) {leftmost = i;} ///setRight and setLeft are used to set the distance of the furthest Enemy in the array in either the right or left direction.

   void setRight(int i) {rightmost = i;} ///setRight and setLeft are used to set the distance of the furthest Enemy in the array in either the right or left direction.
   //Resets the enemy
   void reset() 
   { 
      heading.y=1; origin.x=1596-40*row; origin.y=16+28*column;
      rightmost = 9-column; leftmost = 0+column; dead = 0;
   }

};

#endif

