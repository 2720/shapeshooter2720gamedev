#ifndef GAMESIMULATOR_H
#define GAMESIMULATOR_H
#include "Simulator.h"
#include <memory>
#include "Level1.h"
#include "Player.h"
#include "Title.h"
#include "Level2.h"
#include "Level3.h"
#include <iostream>

///This class serves as the container which determines which level is active, then draws and animates it. 
//Inherits from the Assignment 1 Simulator class.
class GameSimulator: public Simulator {
  private:
   std::list<std::shared_ptr<Level>> llist;
   std::shared_ptr<Player> player;
   std::shared_ptr<Title> title;
   
  public:
   ~GameSimulator(){
      player.reset(); title.reset();
      for (std::list<std::shared_ptr<Level>>::iterator it=llist.begin();
	   it!=llist.end(); ++it)
      {(*it).reset();}
   }
   //COnstructor to iniatilize the values
   // @param Display, int fps
  GameSimulator(const Display &d, int fps) : Simulator(d,fps) {
      std::shared_ptr<Player> p(new Player);
      player = p;
      std::shared_ptr<Level> l1(new Level1(player));
      llist.push_back(l1);
      std::shared_ptr<Level> l2(new Level2(player));
      llist.push_back(l2);
      std::shared_ptr<Level> l3(new Level3(player));
      llist.push_back(l3);
      std::shared_ptr<Title> t(new Title(l1));
      title = t;
      title->setActive();
   }
   
   ///updateModel() calls active level's animation/interactive functions, resets levels which are no longer active.
   void updateModel(double dt){
      if (title->isActive())
      {title->select();}
      else {
	 std::list<std::shared_ptr<Level>>::iterator itr = llist.begin();
	 while (itr != llist.end()) {
	    if ((*itr)->isActive()) {
	       (*itr)->lMove(dt);
	       if ((*itr)->isWon()) 
	       {(*itr)->reset(); itr++;
		  if (itr == llist.end()) {
		     title->setActive(); player->reset();}
		  else {(*itr)->setActive();}}
	       if ((*itr)->isLost())
	       {(*itr)->reset(); title->setActive(); player->reset();}
	    }
	    itr++;
	 }
      }
   }
   ///drawModel() draws active level/title.
   void drawModel(){
      al_clear_to_color(al_map_rgb(0,0,0));
      if (title->isActive())
      {title->draw();}
      else {
	 std::list<std::shared_ptr<Level>>::iterator itr = llist.begin();
	 while (itr != llist.end()) {
	    if ((*itr)->isActive())
	    {(*itr)->lDraw();}
	    itr++;
	 }
      }
      al_flip_display();
   }
};

#endif
