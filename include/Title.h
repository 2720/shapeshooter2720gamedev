#ifndef TITLE_H
#define TITLE_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include "Drawable.h"
#include "Moveable.h"
#include "Level.h"
#include <stdexcept>
///This class handles all drawing and interaction with the main menu.
class Title:public Drawable {
  private:
   bool active, quit;
   int menutimer;
   std::shared_ptr<Level> first;
   ALLEGRO_FONT *font, *f2;
   ALLEGRO_KEYBOARD_STATE st;
  public:
   ~Title() {first.reset();}
  Title(std::shared_ptr<Level> l1) : quit(false), active(true), first(l1) {
      al_init_font_addon();
      al_init_ttf_addon();
      font = al_load_ttf_font("Monoton-Regular.ttf", 90,0);
      if (!font) {
	 throw std::runtime_error("Cannot load 'Monoton-Regular.ttf'");
      }
      f2 = al_load_ttf_font("Bitwise.ttf",40,0);
      if(!f2) {
	 throw std::runtime_error("Cannot load 'Bitwise.ttf'");
      }
   }
   ///draw() draws text to screen with the current selection highlighted.
   void draw(){
      al_draw_text(font,al_map_rgb(0,0,200),800,200,
		   ALLEGRO_ALIGN_CENTRE,"SHAPE SHOOTER");
      if(quit==false){
	 al_draw_text(f2,al_map_rgb(0,200,200),800,500,
		      ALLEGRO_ALIGN_CENTRE,"START");
	 al_draw_text(f2,al_map_rgb(0,100,100),800,575,
		      ALLEGRO_ALIGN_CENTRE,"QUIT");
      }
      else{
	 al_draw_text(f2,al_map_rgb(0,100,100),800,500,
		      ALLEGRO_ALIGN_CENTRE,"START");
	 al_draw_text(f2,al_map_rgb(0,200,200),800,575,
		      ALLEGRO_ALIGN_CENTRE,"QUIT");
      }
   }
   ///select() detects button inputs and performs appropriate operations for the selection.
   void select() {
      al_get_keyboard_state(&st);
      if (quit==false && (al_key_down(&st,ALLEGRO_KEY_UP) ||
			  al_key_down(&st,ALLEGRO_KEY_DOWN))
	                  && menutimer > 5)
      {quit=true; menutimer = 0;}
      else if (quit==true && (al_key_down(&st,ALLEGRO_KEY_UP) ||
			      al_key_down(&st,ALLEGRO_KEY_DOWN))
	                      && menutimer > 5)
      {quit=false; menutimer = 0;}

      if (quit==false && al_key_down(&st,ALLEGRO_KEY_ENTER))
      {active=false; first->setActive();}
      else if (quit==true && al_key_down(&st,ALLEGRO_KEY_ENTER))
      {delete this;}
      menutimer++;
   }
   int menuTimer() {return menutimer;}
   bool isActive() {return active;}
   void setActive() {active=true;}
};
#endif
