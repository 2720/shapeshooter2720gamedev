#ifndef DRAWABLE_H
#define DRAWABLE_H

///This interface handles most objects that need to be drawn to the screen
class Drawable {
  public:
   virtual ~Drawable(){};
   virtual void draw() = 0; //to draw objects on the display
};
#endif
