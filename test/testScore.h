#ifndef __TESTSCORE_H
#define __TESTSCORE_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "Player.h"
///CPPUNIT test class on the Score()functions in the player class
class testScore : public CppUnit::TestFixture {
   CPPUNIT_TEST_SUITE(testScore);
   CPPUNIT_TEST(testGetDefault);
   CPPUNIT_TEST(testAdd200);
   CPPUNIT_TEST(testMultUp);
   CPPUNIT_TEST(testMultUpAdd200);
   CPPUNIT_TEST_SUITE_END();

  private:
   Player* p;

  public:
   //Tests the default score 
   void testGetDefault() {
      p = new Player;
      CPPUNIT_ASSERT(p->getScore()==0);
      delete p;
   }
   //Tests when 200 is added to the schore
   void testAdd200() {
      p = new Player;
      p->addScore(200);
      CPPUNIT_ASSERT(p->getScore()==200);
      delete p;
   }
   //tests the incrementing of the score
   void testMultUp() {
      p = new Player;
      p->multUp();
      CPPUNIT_ASSERT(p->getMult()==2);
      delete p;
   }
   //test the MultUP()function
   void testMultUpAdd200() {
      p = new Player;
      p->multUp(); p->addScore(200);
      CPPUNIT_ASSERT(p->getScore()==400);
      delete p;
   }

};

#endif

   
   
