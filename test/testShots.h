#ifndef __TESTSHOTS_H
#define __TESTSHOTS_H
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "Player.h"

///CPPUNIT test on the shots()functions in the player class
class testShots : public CppUnit::TestFixture {
   CPPUNIT_TEST_SUITE(testShots);
   CPPUNIT_TEST(testGetDefaultShots);
   CPPUNIT_TEST(testAddShot);
   CPPUNIT_TEST(testMaxShots);
   CPPUNIT_TEST_SUITE_END();

  private:
   Player* p;

  public:
   //Function to test the shots(0)
   void testGetDefaultShots() {
      p = new Player;
      CPPUNIT_ASSERT(p->getTotalShots()==3);
      delete p;
   }
   //Function to test the AddShot() funtion in the player class
   void testAddShot() {
      p = new Player;
      p->addShot();
      CPPUNIT_ASSERT(p->getTotalShots()==4);
      delete p;
   }
   //Function to test the MaxShots available
   void testMaxShots() {
      p = new Player;
      p->addShot(); p->addShot(); p->addShot();
      CPPUNIT_ASSERT(p->getTotalShots()==5);
      delete p;
   }

};

#endif
