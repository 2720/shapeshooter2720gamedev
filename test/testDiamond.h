#ifndef __TESTDIAMOND_H
#define __TESTDIAMOND_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "Diamond.h"
#include <memory>


///cppunit test suite Class to test the Diamond class 
class testDiamond : public CppUnit::TestFixture {
   CPPUNIT_TEST_SUITE(testDiamond);
   CPPUNIT_TEST(testDiamondDefaultBotHeight);
   CPPUNIT_TEST(testDiamondBotHeightRow2);
   CPPUNIT_TEST(testDiamondIsDead);
   CPPUNIT_TEST_SUITE_END();

  private:
   std::shared_ptr<Diamond> d;
   std::shared_ptr<Player> p;
      
  public:

   //Function to test the botHeight() in the diamond class
   //return void
   //@param void
   void testDiamondDefaultBotHeight() {
      std::shared_ptr<Player> pl(new Player);
      std::shared_ptr<Diamond> di(new Diamond(p));
      p = pl;
      d = di;
      CPPUNIT_ASSERT(d->botHeight()==1560);
      d.reset();
      p.reset();
      di.reset();
      pl.reset();
   }
    //Function to test the botHeight() in the diamond class
   //return void
   //@param void
   void testDiamondBotHeightRow2() {
      std::shared_ptr<Player> pl(new Player);
      std::shared_ptr<Diamond> di(new Diamond(p,0,1));
      p = pl;
      d = di;
      CPPUNIT_ASSERT(d->botHeight()==1520);
      d.reset();
      p.reset();
      di.reset();
      pl.reset();
   }
    //Function to test the IsDead() in the diamond class
   //return void
   //@param void
   void testDiamondIsDead() {
      std::shared_ptr<Player> pl(new Player);
      std::shared_ptr<Diamond> di(new Diamond(p));
      p = pl;
      d = di;
      CPPUNIT_ASSERT(!d->isDead());
      d.reset();
      p.reset();
      di.reset();
      pl.reset();
   }
};

#endif
      
