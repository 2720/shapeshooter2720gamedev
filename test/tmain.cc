#include <cppunit/ui/text/TestRunner.h>
#include "Diamond.h"
#include "testScore.h"
#include "testShots.h"
#include "testDiamond.h"
#include "testTrapezoid.h"

int main() {
   CppUnit::TextUi::TestRunner runner;
   runner.addTest(testScore::suite());
   runner.addTest(testShots::suite());
   runner.addTest(testDiamond::suite());
   runner.addTest(testTrapezoid::suite());
   runner.run();
   return 0;
}
