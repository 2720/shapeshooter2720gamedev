#ifndef __TESTTRAPEZOID_H
#define __TESTTRAPEZOID_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "Trapezoid.h"
#include <memory>


///TestCase for the Trapezoid enemy object
class testTrapezoid : public CppUnit::TestFixture {
   CPPUNIT_TEST_SUITE(testTrapezoid);
   CPPUNIT_TEST(testTrapezoidDefaultBotHeight);
   CPPUNIT_TEST(testTrapezoidIsDead);
   CPPUNIT_TEST_SUITE_END();

  private:
   std::shared_ptr<Trapezoid> t;
   std::shared_ptr<Player> p;
      
  public:
    //Function to test the DefaultBotHeight() in the trapezoid class
   //@param void 
   //return void
   void testTrapezoidDefaultBotHeight() {
      std::shared_ptr<Player> pl(new Player);
      std::shared_ptr<Trapezoid> tr(new Trapezoid(p));
      p = pl;
      t = tr;
      CPPUNIT_ASSERT(t->botHeight()==1516);
      t.reset();
      p.reset();
      tr.reset();
      pl.reset();
   }

   //Function to test teh  IsDead() function in Trapezoid
   //return void 
   void testTrapezoidIsDead() {
      std::shared_ptr<Player> pl(new Player);
      std::shared_ptr<Trapezoid> tr(new Trapezoid(p));
      p = pl;
      t = tr;
      CPPUNIT_ASSERT(!t->isDead());
      t.reset();
      p.reset();
      tr.reset();
      pl.reset();
   }
};

#endif
      
